import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import scipy.signal
from scipy.signal import butter, filtfilt

#Data config
filename = 'data.csv'
samplings_per_second = 500
time_delay_s = 5.898
do_shorten = True
max_point_to_show = 1000
show_original_alone = True
show_filtered_alone = True

#Plot Names
company = "Company"

# ECG Wave
data = pd.read_csv(filename, sep=',')
time_raw = data['time']
signal_raw = data['voltage']

#Signal from column to array
signal = signal_raw.to_numpy() 

#De-amplification of voltage
signal = signal*(1000/1100)
signal_raw = signal_raw*(1000/1100)

#Reorganize Time
time_raw = time_raw + time_delay_s
time_raw = time_raw * 1000

#Shorten data (optional)
if do_shorten == True:
    signal_raw = signal_raw[:max_point_to_show]
    time_raw = time_raw[:max_point_to_show]
    signal = signal[:max_point_to_show]


#Low Pass Filter
b, a = scipy.signal.butter(4, Wn=30, fs=samplings_per_second) 
LP_Filtered = scipy.signal.filtfilt(b, a, signal)


#Plotting Section
matplotlib.rc('xtick', labelsize=16) 
matplotlib.rc('ytick', labelsize=16) 

#Unique Plotting original
if show_original_alone == True:
    plt.figure(figsize=(13.33,7.5), dpi = 96)
    plt.plot(time_raw, signal_raw, color = 'black', linewidth=2)
    plt.title('Original ' + company + ' ECG Signal', fontsize = 20)
    plt.xlabel('Time (ms)', fontsize=16)
    plt.ylabel('Voltage (mV)', fontsize=16) 
    plt.ylim(0, 3)
    plt.minorticks_on()
    plt.margins(0, .05)
    plt.grid(color='C3', which='both', linewidth=0.5)
    plt.tight_layout()
    plt.show()



#Unique Plotting filtered
if show_filtered_alone == True:
    plt.figure( figsize=(13.33,7.5), dpi = 96)
    plt.plot(time_raw, LP_Filtered, color='black', linewidth=2)
    plt.title('LP Filtered ' + company + ' ECG Signal', fontsize = 20)
    plt.xlabel('Time (ms)', fontsize =16)
    plt.ylabel('Voltage (mV)', fontsize = 16) 
    plt.ylim(0, 3)
    plt.minorticks_on()
    plt.margins(0, .05)
    plt.grid(color='C3', which='both', linewidth=0.5)
    plt.tight_layout() 
    plt.show()



#Comparative Plotting
plt.figure(figsize=(13.33,7.5), dpi = 96)


plt.subplot(211)
plt.plot(time_raw, signal, color='black', linewidth=2)
plt.title('Original ' + company + ' ECG Signal', fontsize = 20)
plt.xlabel('(a) Time (ms)', fontsize = 16)
plt.ylabel('Voltage (mV)', fontsize = 16)
plt.ylim(0, 3)
plt.minorticks_on()
plt.margins(0, .05)
plt.grid(color='C3', which='both', linewidth=0.5)

plt.subplot(212)
plt.plot(time_raw, LP_Filtered, color = 'black', linewidth=2)
plt.title('LP Filtered ' + company + ' ECG Signal', fontsize = 20)
plt.xlabel('(b) Time (ms)', fontsize=16)
plt.ylabel('Voltage (mV)', fontsize=16)
plt.ylim(0, 3)
plt.minorticks_on() 
plt.margins(0, .05)
plt.grid(color='C3', which='both', linewidth=0.5)

plt.tight_layout()
plt.show()
